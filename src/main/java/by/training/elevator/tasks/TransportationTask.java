package by.training.elevator.tasks;

import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Floor;
import by.training.elevator.beans.Passenger;
import by.training.elevator.constants.Constants;
import by.training.elevator.enums.ControllerActions;
import by.training.elevator.enums.Direction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;

import static by.training.elevator.enums.TransportationState.COMPLETED;
import static by.training.elevator.enums.TransportationState.IN_PROGRESS;

/**
 * Class contain task for passengers enter/leave elevator or waiting for signal.
 */
public class TransportationTask implements Runnable {

    /**
     * Logger for this class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(TransportationTask.class);
    /**
     * .
     */
    private final Passenger passenger;

    /**
     * @param passenger .
     */
    public TransportationTask(final Passenger passenger) {
        this.passenger = passenger;
    }

    /**
     * @return passenger.
     */
    public Passenger getPassenger() {
        return passenger;
    }

    /**
     * When thread is in progress, passenger wait a elevator to right direction,
     * ask controller to enter/leave elevator, and the end when passenger is
     * arrived in right destination floor controller set transportation state
     * is completed, then thread is close.
     */
    @Override
    public void run() {
        Controller.reduceCountDownLatch();
        passenger.setTransportationState(IN_PROGRESS);
        while (passenger.getTransportationState() != COMPLETED) {
            waitSignal();
            ControllerActions elevatorAction = Controller.getAction();
            Elevator elevator = Controller.getElevator();
            Floor startFloor = Controller.getFloor(passenger.getStartFloor());
            Lock lockElevator = elevator.getLock();
            Lock lockFloor = startFloor.getLock();
            switch (elevatorAction) {
                case DEBOARDING_OF_PASSENGER:
                    lockElevator.lock();
                    lockFloor.lock();
                    try {
                        leaveElevator();
                    } finally {
                        lockElevator.unlock();
                        lockFloor.unlock();
                    }
                    break;
                case BOARDING_OF_PASSENGER:
                    lockElevator.lock();
                    lockFloor.lock();
                    try {
                        enterElevator();
                    } finally {
                        lockElevator.unlock();
                        lockFloor.unlock();
                    }
                    break;
                default:
                    break;
            }

        }
    }

    /**
     * Passenger wait for signal from controller to wakeup.
     */
    private void waitSignal() {
        int passengerId = passenger.getId();
        Elevator elevator = Controller.getElevator();
        if (elevator.hasPassenger(passengerId)) {
            Lock lockElevator = elevator.getLock();
            lockElevator.lock();
            try {
                elevator.getCondition().await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lockElevator.unlock();
            }
        } else {
            int passengerStartingFloorNumber = passenger.getStartFloor();
            Floor startFloor = Controller.getFloor(passengerStartingFloorNumber);
            Lock lockFloor = startFloor.getLock();
            lockFloor.lock();
            try {
                startFloor.getCondition().await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lockFloor.unlock();
            }
        }
    }


    /**
     * Check passenger is in right floor, then ask controller to
     * leave elevator, logging this process.
     */
    private void leaveElevator() {
        int currentFloorNumber = Controller.getCurrentFloorNumber();
        int passengerId = passenger.getId();
        if (passenger.getDestinationFloor() == currentFloorNumber) {
            LOGGER.info(Controller.getAction()
                    + Constants.BRACKET_OPEN_STRING
                    + passengerId + Constants.ON_FLOOR_STRING + currentFloorNumber
                    + Constants.BRACKET_CLOSE_STRING);
            Controller.removePassengerFromElevator(passengerId);
            Elevator elevator = Controller.getElevator();
            if (!elevator.hasPassenger(passengerId)) {
                Controller.addArrivalPassenger(passenger);
                passenger.setTransportationState(COMPLETED);
                Controller.arrivedPassengers.getAndAdd(1);
            }
            Controller.reduceCountDownLatch();
        }
    }

    /**
     * Check passenger is in right direction of elevator
     * and elevator has free place, then ask controller to enter elevator,
     * logging this process.
     */
    private void enterElevator() {
        Direction passengerMovementDirection = passenger.getDirection();
        Direction elevatorMovementDirection =
                Controller.getMovementDirection();
        if (passengerMovementDirection == elevatorMovementDirection
                && Controller.getElevator().getCurrentCapacity() > 0) {
            int currentFloorNumber = Controller.getCurrentFloorNumber();
            int passengerId = passenger.getId();
            LOGGER.info(Controller.getAction()
                    + Constants.BRACKET_OPEN_STRING
                    + passengerId + Constants.ON_FLOOR_STRING + currentFloorNumber
                    + Constants.BRACKET_CLOSE_STRING);
            Controller.addPassengerToElevator(passenger);
            Elevator elevator = Controller.getElevator();
            if (elevator.hasPassenger(passengerId)) {
                Controller.removePassengerFromFloor(passengerId, passenger.getStartFloor());
            }
            Controller.reduceCountDownLatch();
        }
    }
}

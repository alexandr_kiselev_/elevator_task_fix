package by.training.elevator.enums;

/**
 * Direction to move elevator/passenger
 */
public enum Direction {
    /**
     * Move up.
     */
    UP,
    /**
     * Move down.
     */
    DOWN
}

package by.training.elevator.enums;

/**
 * Enum for controller's actions and logging.
 */
public enum ControllerActions {
    /**
     * On start work controller.
     */
    STARTING_TRANSPORTATION,
    /**
     * On end work controller.
     */
    COMPLETION_TRANSPORTATION,
    /**
     * When controller move elevator.
     */
    MOVING_ELEVATOR,
    /**
     * When controller boarding passenger.
     */
    BOARDING_OF_PASSENGER,
    /**
     * When controller deboarding passenger.
     */
    DEBOARDING_OF_PASSENGER;
}

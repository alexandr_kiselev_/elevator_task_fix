package by.training.elevator.beans;

import by.training.elevator.constants.Constants;
import by.training.elevator.utils.ConfigLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class releases methods for work with building.
 */
public class Building {
    /**
     * Floors number.
     */
    final private int floorsNumber;
    /**
     * Elevator capacity.
     */
    final private int elevatorCapacity;
    /**
     * Passengers number.
     */
    final private int passengersNumber;
    /**
     * Elevator.
     */
    private Elevator elevator;
    /**
     * Passengers list.
     */
    private List<Passenger> passengersList;
    /**
     * Map contain number of floor as key and floor object as value.
     */
    private Map<Integer, Floor> floorsMap;

    /**
     * @return passengers number.
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }

    /**
     * @return floors number.
     */
    public int getFloorsNumber() {
        return floorsNumber;
    }

    /**
     * @return elevator capacity.
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * @return elevator.
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * @param floor number.
     * @return floor.
     */
    public Floor getFloor(final int floor) {
        return floorsMap.get(floor);
    }

    /**
     * @param builder .
     */
    private Building(BuildingBuilder builder) {
        super();
        this.floorsNumber = builder.floorsNumber;
        this.elevatorCapacity = builder.elevatorCapacity;
        this.passengersNumber = builder.passengersNumber;
        this.floorsMap = builder.floorsMap;
        this.elevator = builder.elevator;
        this.passengersList = builder.passengersList;
    }

    //Builder for building.
    public static class BuildingBuilder {
        /**
         * Floors number.
         */
        private int floorsNumber;
        /**
         * Elevator capacity.
         */
        private int elevatorCapacity;
        /**
         * Passengers number.
         */
        private int passengersNumber;
        /**
         * Elevator.
         */
        private Elevator elevator;
        /**
         * Passengers list.
         */
        private List<Passenger> passengersList;
        /**
         * Map contain number of floor as key and floor object as value.
         */
        private Map<Integer, Floor> floorsMap;

        /***
         * Builder with default parameters.
         */
        public BuildingBuilder() {
            super();
            this.floorsNumber = 10;
            this.elevatorCapacity = 7;
            this.passengersNumber = 100;
            this.floorsMap = new HashMap<>();
            this.passengersList = new ArrayList<>();
            this.elevator = new Elevator(elevatorCapacity, Constants.START_FLOOR_FOR_ELEVATOR);
        }

        /**
         * Sets floors number for builder.
         * @param floorsNumber .
         * @return builder.
         */
        public BuildingBuilder setFloorsNumber(int floorsNumber) {
            this.floorsNumber = floorsNumber;
            return this;
        }

        /**
         * Sets elevator capacity for builder.
         * @param elevatorCapacity .
         * @return builder.
         */
        public BuildingBuilder setElevatorCapacity(int elevatorCapacity) {
            this.elevatorCapacity = elevatorCapacity;
            this.elevator = new Elevator(elevatorCapacity, Constants.START_FLOOR_FOR_ELEVATOR);
            return this;
        }

        /**
         * Sets passengers number for builder.
         * @param passengersNumber .
         * @return builder.
         */
        public BuildingBuilder setPassengersNumber(int passengersNumber) {
            this.passengersNumber = passengersNumber;
            return this;
        }

        /**
         * Sets parameters for builder from config file.
         * @return builder.
         */
        public BuildingBuilder setupFromConfig() {
            this.floorsNumber = ConfigLoader.getParamVal(Constants.CONFIG_KEY_FLOORS_NUMBER);
            this.elevatorCapacity = ConfigLoader.getParamVal(Constants.CONFIG_KEY_ELEVATOR_CAPACITY);
            this.passengersNumber = ConfigLoader.getParamVal(Constants.CONFIG_KEY_PASSENGERS_NUMBER);
            this.elevator = new Elevator(ConfigLoader.getParamVal(Constants.CONFIG_KEY_ELEVATOR_CAPACITY),
                    Constants.START_FLOOR_FOR_ELEVATOR);
            return this;
        }

        /**
         * Create floors and passengers for builder.
         * @return builder.
         */
        public BuildingBuilder createFloorsAndPassengers() {
            createFloors();
            createPassengers();
            return this;
        }

        /**
         * Set floors map for builder.
         * @param floorsMap .
         * @return builder.
         */
        public BuildingBuilder setFloors(Map<Integer, Floor> floorsMap) {
            this.floorsMap = floorsMap;
            return this;
        }

        /**
         * Set elevator for builder.
         * @param elevator .
         * @return builder.
         */
        public BuildingBuilder ElevatorSetter(Elevator elevator) {
            this.elevator = elevator;
            return this;
        }

        /**
         * Set passengers list for builder.
         * @param passengersList .
         * @return builder.
         */
        public BuildingBuilder setPassengers(ArrayList<Passenger> passengersList) {

            this.passengersList = passengersList;
            return this;
        }

        /**
         * Put passengers on floors.
         * @return builder.
         */
        public BuildingBuilder putPassengersOnFloorContainers() {
            putPassengersOnFloors();
            return this;
        }

        /**
         * Create building with builder.
         * @return building.
         */
        public Building build() {
            return new Building(this);
        }

        /**
         * Create floors.
         */
        private void createFloors() {
            for (int i = 1; i <= floorsNumber; i++) {
                Floor floor = new Floor(i);
                floorsMap.put(i, floor);
            }
        }

        /**
         * @return random floor's number.
         */
        private int getRandomFloor() {
            Random random = new Random();
            return random.nextInt(floorsNumber) + 1;
        }

        /**
         * Create passengers.
         */
        private void createPassengers() {
            for (int i = 1; i <= passengersNumber; i++) {
                int startFloor = getRandomFloor(); // (i - 1) / 10 + 1; //
                int destinationFloor = getRandomFloor();
                while (startFloor == destinationFloor) {
                    destinationFloor = getRandomFloor();
                }
                Passenger passenger = new Passenger(i,
                        startFloor,
                        destinationFloor);
                passengersList.add(passenger);
            }
        }

        /**
         * Put passengers on dispatch container on floors.
         */
        private void putPassengersOnFloors() {
            for (Passenger passenger : passengersList) {
                int passengerStartFloor = passenger.getStartFloor();
                floorsMap.get(passengerStartFloor).addDispatchPassenger(passenger);
            }
        }


    }

    /**
     * @return passengers list.
     */
    public List<Passenger> getPassengers() {
        return passengersList;
    }

    /**
     * @return floors.
     */
    public Collection<Floor> getFloors() {
        return floorsMap.values();
    }

    /**
     * @param elevator .
     */
    public void setElevator(final Elevator elevator) {
        this.elevator = elevator;
    }

    /**
     * @param floors .
     */
    public void setFloors(final Map<Integer, Floor> floors) {
        this.floorsMap = floors;
    }
}

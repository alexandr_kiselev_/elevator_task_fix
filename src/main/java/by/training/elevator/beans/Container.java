package by.training.elevator.beans;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/***
 * Class realise container for passengers and methods for add/remove.
 */
public class Container {
    /**
     * Map with passenger id as key and passenger as value.
     */
    private ConcurrentHashMap<Integer, Passenger> passengers;

    /**
     * .
     */
    public Container() {
        passengers = new ConcurrentHashMap<>();
    }

    /**
     * Add passenger to container.
     *
     * @param passenger .
     */
    public void addPassenger(final Passenger passenger) {
        int passengerId = passenger.getId();
        passengers.put(passengerId, passenger);
    }

    /**
     * Remove passenger from container.
     *
     * @param passengerId .
     */
    public void removePassenger(final int passengerId) {
        passengers.remove(passengerId);
    }

    /**
     * @return count passengers in container.
     */
    public int getPassengersNumber() {
        return passengers.size();
    }


    /**
     * @return Collection of passengers from container.
     */
    public Collection<Passenger> getPassengers() {
        return passengers.values();
    }


    /**
     * @param passengerId .
     * @return true if container has passengers with passengerId.
     */
    public boolean hasPassenger(final int passengerId) {
        return passengers.containsKey(passengerId);
    }
}

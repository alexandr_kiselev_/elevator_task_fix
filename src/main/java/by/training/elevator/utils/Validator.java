package by.training.elevator.utils;

import by.training.elevator.beans.Building;
import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Floor;
import by.training.elevator.beans.Passenger;
import by.training.elevator.constants.Constants;
import by.training.elevator.enums.TransportationState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Validate complete task or not.
 */
public final class Validator {
    /**
     * .
     */
    private Validator() {
    }

    /**
     * Logger for this class.
     */
    private static final Logger LOGGER = LogManager.getLogger(Validator.class);
    /**
     * Building for get information.
     */
    private static Building building;

    /**
     *
     * @param building .
     */
    public static void setBuilding(final Building building) {
        Validator.building = building;
    }

    /**
     * Check and logging information about complete task.
     *
     * @param building .
     */
    public static void validate(final Building building) {
        Validator.building = building;
        String result;
        if (isAllDispatchContainersEmpty()
                && isElevatorEmpty()
                && isTransportationStateCompleted()
                && isPassengersArrivedToRightFloor()
                && isAllPassengersArrived()) {
            result = Constants.SUCCESSFULLY_STRING;
        } else {
            result = Constants.UNSUCCESSFULLY_STRING;
        }
        LOGGER.info(Constants.VALIDATION_COMPLETED_STRING + result);
    }

    /**
     * Check dispatch containers.
     *
     * @return true if all dispatch containers is empty.
     */
    public static boolean isAllDispatchContainersEmpty() {
        for (Floor floor : building.getFloors()) {
            int floorDispatchPassengersNumber =
                    floor.getDispatchPassengersNumber();
            if (floorDispatchPassengersNumber > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check elevator.
     *
     * @return true if elevator is empty.
     */
    public static boolean isElevatorEmpty() {
        Elevator elevator = building.getElevator();
        if (!elevator.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Check arrived passengers status.
     *
     * @return true all arrived passengers have status "COMPLETED".
     */
    public static boolean isTransportationStateCompleted() {
        for (Floor floor : building.getFloors()) {
            for (Passenger passenger : floor.getArrivalPassengers()) {
                TransportationState passengerTransportationState =
                        passenger.getTransportationState();
                if (passengerTransportationState
                        != TransportationState.COMPLETED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check arrived passengers for right floor.
     *
     * @return true all arrived passengers is arrived on right floor.
     */
    public static boolean isPassengersArrivedToRightFloor() {
        for (Floor floor : building.getFloors()) {
            for (Passenger passenger : floor.getArrivalPassengers()) {
                int passengerDestinationFloor = passenger.getDestinationFloor();
                int floorNumber = floor.getFloorNumber();
                if (passengerDestinationFloor != floorNumber) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check count arrived passengers.
     *
     * @return true count arrived passengers equal all passengers in building.
     */
    public static boolean isAllPassengersArrived() {
        int arrivedPassengers = 0;
        for (Floor floor : building.getFloors()) {
            arrivedPassengers += floor.getArrivalPassengersNumber();
        }
        if (arrivedPassengers != building.getPassengersNumber()) {
            return false;
        }
        return true;
    }
}

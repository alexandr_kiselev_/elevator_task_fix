package by.training.elevator.utils;

import by.training.elevator.constants.Constants;

import java.util.ResourceBundle;

/***
 * Loading config and get values on key.
 */
public final class ConfigLoader {
    /**
     * .
     */
    private ConfigLoader() {
    }

    /**
     * Set config filename.
     */
    private static final ResourceBundle CONFIG_RESOURCE_BUNDLE =
            ResourceBundle.getBundle(Constants.CONFIG);

    /**
     * Get values from config on key.
     *
     * @param key .
     * @return value.
     */
    public static int getParamVal(final String key) {
        return Integer.parseInt(CONFIG_RESOURCE_BUNDLE.getString(key));
    }
}

package by.training.elevator.run;


import by.training.elevator.beans.Building;
import by.training.elevator.tasks.Controller;
import by.training.elevator.beans.Passenger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class for run elevator project.
 */
public final class Runner {

    /**
     *
     */
    private Runner() {

    }

    /**
     * Run controller and passenger's threads.
     *
     * @param args .
     */
    public static void main(final String[] args) {

        Building building = new Building.BuildingBuilder().
                setupFromConfig().
                createFloorsAndPassengers().
                putPassengersOnFloorContainers().
                build();

        Controller controller = new Controller(building);
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(controller);
        for (Passenger passenger : building.getPassengers()) {
            executorService.execute(passenger.getTransportationTask());
        }
        executorService.shutdown();
    }
}

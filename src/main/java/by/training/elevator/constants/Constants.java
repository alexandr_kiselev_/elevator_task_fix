package by.training.elevator.constants;

/**
 * Class contains constant for elevator project.
 */
public final class Constants {
    /**
     *
     */
    private Constants() {
    }

    /**
     * Name of config file.
     */
    public static final String CONFIG = "config";
    /**
     * Key for get from config floors number in building.
     */
    public static final String CONFIG_KEY_FLOORS_NUMBER = "floorsNumber";
    /**
     * Key for get from config elevator capacity in building.
     */
    public static final String CONFIG_KEY_ELEVATOR_CAPACITY = "elevatorCapacity";
    /**
     * Key for get from config passengers number in building.
     */
    public static final String CONFIG_KEY_PASSENGERS_NUMBER = "passengersNumber";
    /**
     * Start floor for elevator.
     */
    public static final int START_FLOOR_FOR_ELEVATOR = 1;
    /**
     * String for logging move elevator "from floor".
     */
    public static final String FROM_FLOOR_STRING = "(from floor-";
    /**
     * String for logging move elevator "to floor".
     */
    public static final String TO_FLOOR_STRING = " to floor-";
    /**
     * String for logging boarding/deboarding passengers.
     */
    public static final String ON_FLOOR_STRING = " on floor-";
    /**
     * String "open bracket".
     */
    public static final String BRACKET_OPEN_STRING = " (";
    /**
     * String "close bracket".
     */
    public static final String BRACKET_CLOSE_STRING = ")";
    /**
     * String when validation is complete.
     */
    public static final String VALIDATION_COMPLETED_STRING = "Validation completed ";
    /**
     * String result of validation "successfully".
     */
    public static final String SUCCESSFULLY_STRING = "successfully";
    /**
     * String result of validation "unsuccessfully".
     */
    public static final String UNSUCCESSFULLY_STRING = "unsuccessfully";
    /**
     * Timeout for wait passengers in milliseconds.
     */
    public static final long TIMEOUT = 1000;
}

package by.training.elevator;

import by.training.elevator.beans.Building;
import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Floor;
import by.training.elevator.beans.Passenger;
import by.training.elevator.constants.Constants;
import by.training.elevator.enums.TransportationState;
import by.training.elevator.utils.ConfigLoader;
import by.training.elevator.utils.Validator;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertTrue;

public class TestValidation {
    private Building building;

    /**
     * Prepare.
     */
    @Before
    public void setup() {
        building = new Building.BuildingBuilder().
                setupFromConfig().
                createFloorsAndPassengers().
                putPassengersOnFloorContainers().
                build();

        Validator.setBuilding(building);
    }

    /**
     * Check for transportation completed.
     */
    @Test
    public void returnCompletedAsTransportationStateOfEachPassengers() {
        final int floorNumber = 1;
        Passenger passenger = mock(Passenger.class);
        when(passenger.getTransportationState()).thenReturn(TransportationState.COMPLETED);
        Floor floor = mock(Floor.class);
        floor.addArrivalPassenger(passenger);
        Map<Integer, Floor> floors = new HashMap<>();
        floors.put(floorNumber, floor);
        building.setFloors(floors);
        boolean isTransportationStateCompleted = Validator.isTransportationStateCompleted();
        assertTrue("Transportation completed", isTransportationStateCompleted);
    }

    /**
     * Check passengers arrived to right floor.
     */
    @Test
    public void checkIfPassengersDestinationFloorNumberSameAsArrivalContainer() {
        final int floorNumber = 1;
        Passenger passenger = mock(Passenger.class);
        when(passenger.getDestinationFloor()).thenReturn(floorNumber);
        Floor floor = mock(Floor.class);
        floor.addArrivalPassenger(passenger);
        Map<Integer, Floor> floors = new HashMap<>();
        floors.put(floorNumber, floor);
        building.setFloors(floors);
        boolean isPassengersArrivedToRightFloor = Validator.isPassengersArrivedToRightFloor();
        assertTrue("Passengers arrived to right floor", isPassengersArrivedToRightFloor);
    }

    /**
     * Check for empty dispatch containers.
     */
    @Test
    public void checkIfAllDispatchContainersAreEmpty() {
        final int floorNumber = 1;
        final int dispatchPassengersNumber = 0;
        Floor floor = mock(Floor.class);
        when(floor.getDispatchPassengersNumber())
                .thenReturn(dispatchPassengersNumber);
        Map<Integer, Floor> floors = new HashMap<>();
        floors.put(floorNumber, floor);
        building.setFloors(floors);
        boolean isAllDispatchContainersEmpty =
                Validator.isAllDispatchContainersEmpty();
        assertTrue("All dispatch containers are empty", isAllDispatchContainersEmpty);
    }

    /**
     * Check for empty elevator.
     */
    @Test
    public void checkIfElevatorIsEmpty() {
        Elevator elevator = mock(Elevator.class);
        when(elevator.isEmpty()).thenReturn(true);
        building.setElevator(elevator);
        boolean isElevatorEmpty = Validator.isElevatorEmpty();
        assertTrue("Elevator is empty", isElevatorEmpty);
    }

    /**
     * Check passengers is arrived.
     */
    @Test
    public void checkIfTotalNumberOfPeopleInAllArrivalFloorContainersSameAsPassengersNumber() {
        final int floorNumber = 1;
        Floor floor = mock(Floor.class);
        when(floor.getArrivalPassengersNumber()).thenReturn(building.getPassengersNumber());
        Map<Integer, Floor> floors = new HashMap<>();
        floors.put(floorNumber, floor);
        building.setFloors(floors);
        boolean isAllPassengersArrived = Validator.isAllPassengersArrived();
        assertTrue("All passengers arrived", isAllPassengersArrived);
    }
}

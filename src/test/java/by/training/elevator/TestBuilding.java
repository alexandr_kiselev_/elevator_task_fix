package by.training.elevator;

import by.training.elevator.beans.Building;
import by.training.elevator.beans.Elevator;
import by.training.elevator.beans.Floor;
import by.training.elevator.beans.Passenger;


import by.training.elevator.constants.Constants;
import by.training.elevator.utils.ConfigLoader;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestBuilding {
    private Building building;

    /**
     * Prepare.
     */
    @Before
    public void setup() {
        building = new Building.BuildingBuilder().
                setFloorsNumber(9).
                setElevatorCapacity(5).
                setPassengersNumber(88).
                createFloorsAndPassengers().
                putPassengersOnFloorContainers().
                build();

    }
    /**
     * Check for count floors and passengers.
     */
    @Test
    public void createsSpecifiedNumberFloorsAndPassengers() {
        int floorsNumber = building.getFloors().size();
        assertThat("Creates specified floors number",
                floorsNumber, is(building.getFloorsNumber()));
        int passengersNumber = building.getPassengers().size();
        assertThat("Creates specified passengers number",
                passengersNumber, is(building.getPassengersNumber()));
    }

    /**
     * Check passengers have unique id.
     */
    @Test
    public void createPassengersWithUniqueId() {
        for (Passenger passenger : building.getPassengers()) {
            int passengerId = passenger.getId();
            int sameId = 0;
            for (Passenger comparePassenger : building.getPassengers()) {
                int comparePassengerId = comparePassenger.getId();
                if (passengerId == comparePassengerId) {
                    sameId++;
                }
            }
            assertThat("Each id is unique", sameId, is(1));
        }
    }

    /**
     * Check passengers destination floor not equal start floor .
     */
    @Test
    public void createPassengersWithSourceFloorIsNotEqualToTargetFloor() {
        for (Passenger passenger : building.getPassengers()) {
            int passengerSourceFloor = passenger.getStartFloor();
            int passengerTargetFloor = passenger.getDestinationFloor();
            assertThat("Start and destination floors is not equal",
                    passengerSourceFloor, not(passengerTargetFloor));
        }
    }

    /**
     * Check passenger's number.
     */
    @Test
    public void returnsPassengersCountSameAsContains() {
        int returnPassengersCount = building.getPassengersNumber();
        int containsPassengersCount = building.getPassengers().size();
        assertThat("Returns passengers count same as contains",
                returnPassengersCount, is(containsPassengersCount));
    }
    /**
     * Check floors's number.
     */
    @Test
    public void returnsFloorsCountSameAsContains() {
        int returnFloorsCount = building.getFloorsNumber();
        int containsFloorsCount = building.getFloors().size();
        assertThat("Returns floors count same as contains",
                returnFloorsCount, is(containsFloorsCount));
    }
    /**
     * Check passenger's is on start floor.
     */
    @Test
    public void checksIfThereArePassengersOnSourceFloors() {
        for (Floor floor : building.getFloors()) {
            int floorNumber = floor.getFloorNumber();
            for (Passenger passenger : floor.getDispathPassengers()) {
                int passengerSourceNumber = passenger.getStartFloor();
                assertThat("Passenger is on start floor",
                        passengerSourceNumber, is(floorNumber));
            }
        }
    }
    /**
     * Check elevator capacity.
     */
    @Test
    public void returnsElevatorCapacitySameAsContains() {
        Elevator elevator = building.getElevator();
        int returnElevatorCapacity = elevator.getCurrentCapacity();
        assertThat("Elevator capacity same as contains",
                returnElevatorCapacity, is(building.getElevatorCapacity()));
    }
    /**
     * Check count transported passengers.
     */
    @Test
    public void returnsTransportedPassengersCount() {
        Elevator elevator = building.getElevator();
        for (int i = 0; i < building.getElevatorCapacity(); i++) {
            Passenger passenger = mock(Passenger.class);
            when(passenger.getId()).thenReturn(i);
            elevator.addPassenger(passenger);
        }
        int elevatorPassengersNumber = elevator.getPassengers().size();
        assertThat("Return transported passengers count",
                elevatorPassengersNumber, is(building.getElevatorCapacity()));
    }
}
